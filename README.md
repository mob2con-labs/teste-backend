# Teste Backend
Olá candidato(a)!

O objetivo desse teste é avaliar sua proficiência em algumas áreas chaves do seu dia-a-dia de desenvolvedor backend aqui na Mob2con, trabalhando com:
* Autenticação para Arquitetura stateless (visando escalabilidade)
* Boas praticas de RESTful apis
* Integridade e Organização de código
* Conteinerização (é um diferencial)

Estruturamos um teste pra você sentir um gostinho da temática da Mob2con, que trabalha, principalmente, com o controle de **visitantes** para **redes varejistas**. Mas, não se preocupe, o cenário do teste é só um recorte da realidade, sem aplicação na solução final.

## Antes de começar...
Tenha em mente que sua aplicação deve utilizar:
* JWT para autenticação
* Banco relacional, de preferência, postgres
* Testes automatizados
* GIT com commits descritivos e pequenos

## As Histórias
A atividade consiste em desenvolver apenas a API, não há necessidade de criar um front para ela.
1. Como administrador, preciso gerenciar Redes Varejistas (crud) para dar entrada em novos clientes.
2. Como administrador, preciso pesquisar redes varejistas pelo nome para ter acesso as informações.
3. Como administrador, preciso gerenciar visiantes (crud) de cada rede para que mais pessoas utilizem o sistema.
4. Como Administrador, preciso enviar fotos dos visitantes para identificá-los.
5. Como Administrador, preciso pesquisar visitantes por nome para ter acesso as informações.
6. Como usuário da rede, preciso me logar no sistema para fazer operações.
7. Como usuário da rede, preciso me deslogar no sistema para impedir que outros se passem por mim.
8. Como usuário da rede, preciso registrar entradas e saidas de visitantes.
9. Como usuário externo, não autenticado, preciso do número total de redes e seus visitantes. 

## Entidades
Usuário
* nome (texto)
* usuário (texto)
* senha (texto)
* rede (Rede) 

Rede 
* cnpj (string)
* nome (string)

Visitante 
* nome (texto)
* rede (Rede)


## Então...

É isso... Sem muito mistério, certo? Mas vamos a alguns diferenciais.
* Readme com as instruções de como subir seu projeto
* Uso de boas práticas de desenvolvimento com RoR
* Dockerização
* [GitFlow workflow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow)

## Para resolver o exercício
Clone este repositório, crie uma branch para você desenvolver sua resposta e ao terminar, faça um pull request.

Muito Obrigado!